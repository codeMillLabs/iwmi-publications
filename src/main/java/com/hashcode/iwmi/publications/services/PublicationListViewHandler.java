/*
 * FILENAME
 *     PublicationListViewHandler.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import static com.hashcode.iwmi.publications.data.SortType.DESC;
import static com.hashcode.iwmi.publications.services.IWMISolrRequestHandler.createHandler;
import static com.hashcode.iwmi.publications.util.IWMIConst.DATA_SET;
import static com.hashcode.iwmi.publications.util.IWMIConst.DOCTYPE_FILTER_PLACEHOLDER;
import static com.hashcode.iwmi.publications.util.IWMIConst.SEARCH_TEXT;
import static com.hashcode.iwmi.publications.util.IWMIConst.SEARCH_TEXT_PLACEHOLDER;
import static com.hashcode.iwmi.publications.util.IWMIConst.START_POINT;
import static com.hashcode.iwmi.publications.util.IWMIUtils.getFormattedDate;
import static com.hashcode.iwmi.publications.util.IWMIUtils.getListViewRowCount;
import static com.hashcode.iwmi.publications.util.IWMIUtils.isNullOrEmpty;
import static java.lang.Long.parseLong;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.publications.data.FieldDetail;
import com.hashcode.iwmi.publications.util.DocumentType;
import com.hashcode.iwmi.publications.util.IWMIConst;
import com.hashcode.iwmi.publications.util.IWMIUtils;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Publication Details List view handler.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class PublicationListViewHandler extends AbstractViewHandler
{

    private static final Logger logger = LoggerFactory.getLogger(PublicationListViewHandler.class);

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.publications.services.AbstractViewHandler#handleRequest(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response, DocumentType docType)
        throws Exception
    {
        logger.debug("Request came to Publication List view handler to process");

        String searchText = request.getParameter(SEARCH_TEXT.getDesc());
        String startPoint = request.getParameter(START_POINT.getDesc());
        searchText = (isNullOrEmpty(searchText)) ? "*" : searchText;
        startPoint = (isNullOrEmpty(startPoint)) ? "0" : startPoint;
        
        String sortKey = "id " + DESC.getDesc();
        

        long startPosition = (isNullOrEmpty(startPoint)) ? 0 : parseLong(startPoint);

        int maxRowCount = getListViewRowCount();

        IWMISolrRequestHandler handler = createHandler();

        String solrQueryParams = createSolrQueryParam(searchText, docType);

        String solrQuery = handler.createQuery(solrQueryParams, sortKey, startPosition, maxRowCount);

        QueryResponse queryResponse = handler.querySolr(solrQuery);
        SolrDocumentList docList = handler.getDocuments(queryResponse);

        long totalNoRecs = handler.noOfRecordsFound(docList);
        long startingPoint = handler.getStartingPosition(docList);

        logger.info(
            "Publication List View handler processed successfully, [ Total No Records : {}, Starting Point: {}]",
            totalNoRecs, startingPoint);

        setMandatoryAttributes(request, searchText, "" + docType.getId(), sortKey);
        setDataInRequestAttribute(request, docList, totalNoRecs, startingPoint);
        setPaginationAttributes(request, null, maxRowCount, totalNoRecs, startingPoint);

        logger.info("Publication List View handler processed successfully");
    }

    private String createSolrQueryParam(String searchText, DocumentType docType)
    {
        String queryStr = IWMIUtils.getPropertyValue("pub.solr.search.basic.query.template");
        queryStr = queryStr.replaceAll(DOCTYPE_FILTER_PLACEHOLDER.getDesc(), docType.getQuery());
        queryStr = queryStr.replaceAll(SEARCH_TEXT_PLACEHOLDER.getDesc(), searchText);
        logger.info("Solr Request query : [ {} ]", queryStr);
        return queryStr;
    }

    @Override
    protected void setDataSet(HttpServletRequest request, SolrDocumentList docList)
    {
        List<Map<String, FieldDetail>> displayDataSet = new ArrayList<Map<String, FieldDetail>>();
        for (SolrDocument doc : docList)
        {
            Map<String, FieldDetail> documentDataMap = getDataMap(doc);
            logger.debug("Document Map Detail, {}", documentDataMap);
            displayDataSet.add(documentDataMap);
        }

        if (displayDataSet.isEmpty())
        {
            request.setAttribute(IWMIConst.NO_DATA.getDesc(), "No-Data");
        }

        logger.info("Display Data set count: {}", displayDataSet.size());
        request.setAttribute(DATA_SET.getDesc(), displayDataSet);
    }

    @SuppressWarnings("unchecked")
    private Map<String, FieldDetail> getDataMap(SolrDocument doc)
    {
        Map<String, FieldDetail> dataMap = new HashMap<String, FieldDetail>(doc.getFieldNames().size());
        for (String fieldName : doc.getFieldNames())
        {
            Object value = doc.getFieldValue(fieldName);
            String fieldValue = null;

            if (null == value)
                continue;
            logger.debug("Field Detail -- > [ name :{}, type :{}, value :{}]", new Object[] {
                fieldName, value.getClass().getName(), value
            });

            if (value instanceof String)
            {
                String fldValue = (String) value;
                fldValue = (fldValue.startsWith(",")) ? fldValue.substring(1) : fldValue;
                if (isNullOrEmpty(fldValue))
                    continue;
                else
                    fieldValue = fldValue;
            }
            else if (value instanceof Date)
            {
                fieldValue = getFormattedDate((Date) value);
            }
            else if (value instanceof List)
            {
                List<String> datalist = (List<String>) value;
                StringBuilder sbuffer = new StringBuilder();

                for (String listVal : datalist)
                {
                    if (!isNullOrEmpty(listVal))
                    {
                        sbuffer.append(listVal).append(", ");
                    }
                }
                fieldValue = sbuffer.toString();
            }
            FieldDetail field = FieldDetail.createFieldDetail(fieldName, fieldValue);
            if (null != field)
                dataMap.put(fieldName, field);
        }
        return dataMap;
    }

}
