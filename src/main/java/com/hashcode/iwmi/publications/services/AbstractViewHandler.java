/*
 * FILENAME
 *     AbstractViewHandler.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.services;

import static com.hashcode.iwmi.publications.util.IWMIConst.CURRENT_PG;
import static com.hashcode.iwmi.publications.util.IWMIConst.DATA_SET_SIZE;
import static com.hashcode.iwmi.publications.util.IWMIConst.DOC_TYPE;
import static com.hashcode.iwmi.publications.util.IWMIConst.FIELD_ORDER;
import static com.hashcode.iwmi.publications.util.IWMIConst.SEARCH_TEXT;
import static com.hashcode.iwmi.publications.util.IWMIConst.SORT_KEY;
import static com.hashcode.iwmi.publications.util.IWMIConst.START_POINT;
import static com.hashcode.iwmi.publications.util.IWMIConst.TOTAL_NO_REC;
import static com.hashcode.iwmi.publications.util.IWMIConst.TOTAL_PGS;
import static com.hashcode.iwmi.publications.util.IWMIUtils.getCurrentPage;
import static com.hashcode.iwmi.publications.util.IWMIUtils.getFieldOrder;
import static com.hashcode.iwmi.publications.util.IWMIUtils.getTotalNoPages;
import static com.hashcode.iwmi.publications.util.IWMIUtils.isNullOrEmpty;
import static com.hashcode.iwmi.publications.util.IWMIUtils.setNextPage;
import static com.hashcode.iwmi.publications.util.IWMIUtils.setPrevPage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.hashcode.iwmi.publications.util.DocumentType;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Abstract View handler class handle view based on the request.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
 **/
public abstract class AbstractViewHandler
{

    public AbstractViewHandler()
    {

    }

    /**
     * Handle all the requests by the respective handler implementations
     * 
     * @param request
     *            http request
     * @param response
     *            http response
     * @param docType
     *            document type
     * @throws Exception
     *             exception in any error
     */
    public abstract void handleRequest(final HttpServletRequest request, final HttpServletResponse response,
        final DocumentType docType) throws Exception;

    /**
     * <p>
     * Setter Required attributes for every request.
     * </p>
     * 
     * 
     * @param request
     *            Http Servlet Request
     * @param searchText
     *            searchText
     * @param docType
     *            docType
     * @param sortKey
     *            sortKey
     * 
     */
    protected void setMandatoryAttributes(final HttpServletRequest request, String searchText,
        final String docType, final String sortKey)
    {
        searchText = (isNullOrEmpty(searchText) || searchText.equals("*")) ? "" : searchText;
        request.setAttribute(SEARCH_TEXT.getDesc(), searchText);
        request.setAttribute(DOC_TYPE.getDesc(), docType);
        request.setAttribute(SORT_KEY.getDesc(), sortKey);
    }

    protected void setDataInRequestAttribute(final HttpServletRequest request, final SolrDocumentList docList,
        long totalNoRecs, long startingPoint)
    {
        request.setAttribute(TOTAL_NO_REC.getDesc(), totalNoRecs);
        request.setAttribute(START_POINT.getDesc(), startingPoint);
        setDataSet(request, docList);
        request.setAttribute(DATA_SET_SIZE.getDesc(), docList.size());
        request.setAttribute(FIELD_ORDER.getDesc(), getFieldOrder());
    }

    /**
     * <p>
     * Implementation should fill with the data population.
     * </p>
     * 
     * @param request
     *            http request
     * @param docList
     *            {@link SolrDocument} data set
     */
    protected abstract void setDataSet(final HttpServletRequest request, final SolrDocumentList docList);

    /**
     * <p>
     * Setter Pagination related attributes.
     * </p>
     * 
     * @param request
     *            http request
     * @param showAll
     *            show all feature
     * @param maxRowCount
     *            max row count per page
     * @param totalNoRecs
     *            total no of records found
     * @param startingPoint
     *            starting point of the page
     */
    protected void setPaginationAttributes(final HttpServletRequest request, final String showAll,
        final int maxRowCount, final long totalNoRecs, final long startingPoint)
    {
        request.setAttribute(TOTAL_PGS.getDesc(), getTotalNoPages(totalNoRecs, maxRowCount));
        request.setAttribute(CURRENT_PG.getDesc(), getCurrentPage(startingPoint, maxRowCount));
        setNextPage(request, totalNoRecs, startingPoint, maxRowCount);
        setPrevPage(request, totalNoRecs, startingPoint, maxRowCount);
    }

}
