
package com.hashcode.iwmi.publications.util;

/**
 * <p>
 * System wide constants 
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public enum IWMIConst {
	
	TOTAL_NO_REC("total_no_recs"),
	DATA_SET("data_set"),
	SEARCH_TEXT("st"),
	DOC_TYPE("dt"),
	SORT_KEY("sk"),
	FIELD_ORDER("field_order"),
    DATA_SET_SIZE("data_set_size"),
    DATA_LIST_ROW_COUNT("datalst_row_count"),
	CURRENT_PG("currentPg"),
	PRE_PG("prevPg"),
	HAS_PREV_PG("hasPrevPg"),
	NEXT_PG("nextPg"),
	HAS_NEXT_PG("hasNextPg"),
	TOTAL_PGS("totalPg"),
	NO_DATA("no_data"),
	START_POINT("sp"),
	SHOW_ALL("sa"),
	DOCTYPE_FILTER_PLACEHOLDER("\\{docType_filter\\}"),
	SEARCH_TEXT_PLACEHOLDER("\\{searchText\\}");
	
	private String desc;
	
	private IWMIConst(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}
	
}
