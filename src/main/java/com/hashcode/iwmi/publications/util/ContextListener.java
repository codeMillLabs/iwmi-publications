package com.hashcode.iwmi.publications.util;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		Properties properties = new Properties();
		try {
			properties.load(ContextListener.class.getClassLoader().getResourceAsStream("iwmi-publications.properties"));
			arg0.getServletContext().setAttribute("properties", properties);
		} catch (IOException e) {
		}
	}

}
