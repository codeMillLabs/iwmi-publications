/**
 * 
 */
package com.hashcode.iwmi.publications.util;

import static com.hashcode.iwmi.publications.util.IWMIConst.HAS_NEXT_PG;
import static com.hashcode.iwmi.publications.util.IWMIConst.HAS_PREV_PG;
import static com.hashcode.iwmi.publications.util.IWMIConst.NEXT_PG;
import static com.hashcode.iwmi.publications.util.IWMIConst.PRE_PG;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Utility class of the system
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amilasilva@hashcodesys.com
 * @version 1.0
 */
public final class IWMIUtils
{

    private static final Logger logger = LoggerFactory.getLogger(IWMIUtils.class);
    private static final String propFile = "./iwmi-publications.properties";
    private static Properties properties;
    private static Long milisIn1980Yr = get1980Year();

    static
    {
        try
        {
            properties = new Properties();
            properties.load(IWMIUtils.class.getClassLoader().getResourceAsStream(propFile));
            logger.info("IWMI properties loaded successfully");
        }
        catch (IOException e)
        {
            logger.error("Error encountered in loading the properties,", e);
        }
    }

    /**
     * <p>
     * Get Value for given property
     * </p>
     * 
     * @param key
     *            property
     * @return value
     */
    public static String getPropertyValue(final String key)
    {

        if (null != key)
            return properties.getProperty(key);
        else
            return null;
    }

    /**
     * <p>
     * Get Max Row count for show all system wide
     * </p>
     * 
     * @return row count
     */
    public static int getMaxRowShowAllCount()
    {
        return Integer.parseInt(properties.getProperty("pub.max.result.show.all.row.count"));
    }

    /**
     * <p>
     * Get row count for list view.
     * </p>
     * 
     * @return row count
     */
    public static int getListViewRowCount()
    {
        return Integer.parseInt(properties.getProperty("pub.max.result.list.view.row.count"));
    }

    /**
     * <p>
     * Set next page starting pointer and the flag
     * </p>
     * 
     * @param request
     *            {@link HttpServletRequest}
     * @param totalNoRec
     *            total no of records
     * @param startingPoint
     *            starting point
     * @param rowCount
     *            max row count to display
     */
    public static void setNextPage(final HttpServletRequest request, final long totalNoRec, final long startingPoint,
        final int rowCount)
    {
        long nextStartingPoint = startingPoint + rowCount;
        if (totalNoRec > nextStartingPoint)
        {
            request.setAttribute(HAS_NEXT_PG.getDesc(), Boolean.TRUE);
            request.setAttribute(NEXT_PG.getDesc(), nextStartingPoint);
        }
        else
        {
            request.setAttribute(HAS_NEXT_PG.getDesc(), Boolean.FALSE);
        }
    }

    /**
     * <p>
     * Set prev page starting pointer and the flag
     * </p>
     * 
     * @param request
     *            {@link HttpServletRequest}
     * @param totalNoRec
     *            total no of records
     * @param startingPoint
     *            starting point
     * @param rowCount
     *            max row count to display
     */
    public static void setPrevPage(final HttpServletRequest request, final long totalNoRec, final long startingPoint,
        final int rowCount)
    {
        long prevStartingPoint = startingPoint - rowCount;
        if (prevStartingPoint >= 0)
        {
            request.setAttribute(HAS_PREV_PG.getDesc(), Boolean.TRUE);
            request.setAttribute(PRE_PG.getDesc(), prevStartingPoint);
        }
        else
        {
            request.setAttribute(HAS_PREV_PG.getDesc(), Boolean.FALSE);
        }
    }

    /**
     * <p>
     * Get the total no of pages available for search
     * </p>
     * 
     * @param totalNoRec
     *            total no of records
     * @param rowCount
     *            max row count to display
     * @return no of pages
     */
    public static int getTotalNoPages(final long totalNoRec, final int rowCount)
    {
        return (int) ((totalNoRec % rowCount == 0) ? totalNoRec / rowCount : (totalNoRec / rowCount) + 1);
    }

    /**
     * <p>
     * Get the current page no.
     * </p>
     * 
     * @param startingPoint
     *            starting point
     * @param rowCount
     *            max row count to display
     * @return current page no
     * 
     */
    public static int getCurrentPage(final long startingPoint, final int rowCount)
    {
        return (int) (startingPoint / rowCount + 1);
    }

    /**
     * <p>
     * Check for String value is null or empty.
     * </p>
     * 
     * @param str
     *            value to check
     * @return true if value is null or empty otherwise false
     */
    public static boolean isNullOrEmpty(final String str)
    {
        return (null == str || str.isEmpty()) ? true : false;
    }

    /**
     * <p>
     * Format the date according the system wide format.
     * </p>
     * 
     * @param date
     *            date to be formatted
     * @return formatted date in String format
     */
    public static String getFormattedDate(final Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(getPropertyValue("pub.system.date.format"));
        return sdf.format(date);
    }

    /**
     * <p>
     * Get the Field order of the Project details.
     * </p>
     * 
     * @return List of fields
     */
    public static List<String> getFieldOrder()
    {
        String fieldsStr = getPropertyValue("pub.fields.order");
        List<String> fieldList = new ArrayList<String>();

        StringTokenizer tokenizer = new StringTokenizer(fieldsStr, ",");
        while (tokenizer.hasMoreTokens())
            fieldList.add(tokenizer.nextToken().trim());
        return fieldList;
    }

    private static Long get1980Year()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 1980);
        return calendar.getTimeInMillis();
    }

    public static String getFormattedTitle(final String rawTitle)
    {
        if (isNullOrEmpty(rawTitle))
            return "...";
        else
            return filterNonASCII(rawTitle);
    }

    /**
     * <p>
     * Filter out the non ASCII characters from the string.
     * </p>
     * 
     * @param value
     *            string with non ASCII
     * @return string with out ASCII
     */
    public static String filterNonASCII(String value)
    {
        return value.replaceAll("[^\\x00-\\x7f]", "");
    }

    public static void main(String[] args)
    {
        String serverUrl = IWMIUtils.getPropertyValue("pub.solr.server.url");
        logger.debug("Solr Server :" + serverUrl);

    }
}
