/*
 * FILENAME
 *     DocumentType.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.util;

import static com.hashcode.iwmi.publications.util.IWMIUtils.getPropertyValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Class to hold the Document Types.
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amilasilva@hashcodesys.com
 * @version 1.0
 **/
public class DocumentType implements Comparable<DocumentType>
{
    private int id;
    private String dispName;
    private int sortOrder;
    private boolean isChild;
    private String query;
    private List<DocumentType> subTypes;

    private static List<DocumentType> docTypes = new ArrayList<DocumentType>();
    private static Map<Integer, DocumentType> docTypeMap = new HashMap<Integer, DocumentType>();

    private static final Logger logger = LoggerFactory.getLogger(DocumentType.class);

    private DocumentType(int id, String displayName, String query)
    {
        this.id = id;
        this.dispName = displayName;
        this.sortOrder = id;
        this.query = query;
    }

    private DocumentType(int id, String displayName, DocumentType... subTypes)
    {
        this.id = id;
        this.dispName = displayName;
        this.sortOrder = id;
        this.isChild = false;

        if (subTypes != null)
            this.subTypes = Arrays.asList(subTypes);
    }

    /**
     * <p>
     * Load all the document type data from the configuration.
     * </p>
     * 
     */
    public static void loadData()
    {
        if (!docTypes.isEmpty())
            return;

        int totalDocTypes = Integer.valueOf(getPropertyValue("total.no.doc.types"));
        logger.info("Total no of Document Types :{}", totalDocTypes);

        for (int i = 1; i <= totalDocTypes; i++)
        {
            String docTypeDtl = getPropertyValue("doc.type." + i);
            String[] values = docTypeDtl.split(",", -1);

            if (values != null && values.length >= 4)
            {
                if ("0".equals(values[1].trim()))
                { // check has children 0- no children, 1 - has children
                    int id = Integer.valueOf(values[0].trim());
                    String displayName = values[2].trim();
                    String query = values[3].trim();

                    DocumentType docType = new DocumentType(id, displayName, query);
                    docTypeMap.put(id, docType);

                }
                else if ("1".equals(values[1].trim()))
                {
                    int id = Integer.valueOf(values[0].trim());
                    String displayName = values[2].trim();
                    String subDocTypes = values[3].trim().substring(1, (values[3].trim().length() - 1));
                    String[] docTypeIds = subDocTypes.split(";", -1);

                    DocumentType docType = new DocumentType(id, displayName);
                    List<DocumentType> subTypes = new ArrayList<DocumentType>();

                    for (int k = 0; k < docTypeIds.length; k++)
                    {
                        int subDocId = Integer.parseInt(docTypeIds[k].trim());
                        DocumentType subDocType = docTypeMap.get(subDocId);
                        subDocType.setChild(true);
                        subTypes.add(subDocType);
                    }
                    docType.setSubTypes(subTypes);
                    docTypeMap.put(id, docType);
                }
            }
        }
        docTypes = sortDocumentTypes(docTypeMap);
        logger.info("Document type data loaded sucessfully");
    }

    /**
     * <p>
     * Gives the list of ordered main Document types according to the specified sort order.
     * </p>
     * 
     * @return List of {@link DocumentType}
     * 
     */
    private static List<DocumentType> sortDocumentTypes(Map<Integer, DocumentType> docTypeMap)
    {
        //sort document types
        List<DocumentType> tempDocList = new ArrayList<DocumentType>();
        for (Map.Entry<Integer, DocumentType> entry : docTypeMap.entrySet())
        {
            DocumentType docType = entry.getValue();
            if (!docType.isChild)
            {
                if (!docType.getSubTypes().isEmpty())
                {
                    Collections.sort(docType.getSubTypes());
                }
                tempDocList.add(entry.getValue());
            }
        }

        Collections.sort(tempDocList);
        return tempDocList;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for dispName.
     * </p>
     * 
     * @return the dispName
     */
    public String getDispName()
    {
        return dispName;
    }

    /**
     * <p>
     * Getter for sortOrder.
     * </p>
     * 
     * @return the sortOrder
     */
    public int getSortOrder()
    {
        return sortOrder;
    }

    /**
     * <p>
     * Getter for isChild.
     * </p>
     * 
     * @return the isChild
     */
    public boolean isChild()
    {
        return isChild;
    }

    /**
     * <p>
     * Setting value for isChild.
     * </p>
     * 
     * @param isChild
     *            the isChild to set
     */
    public void setChild(boolean isChild)
    {
        this.isChild = isChild;
    }

    /**
     * <p>
     * Getter for subTypes.
     * </p>
     * 
     * @return the subTypes
     */
    public List<DocumentType> getSubTypes()
    {
        if (subTypes == null)
        {
            subTypes = new ArrayList<DocumentType>();
        }
        return subTypes;
    }

    /**
     * <p>
     * Setting value for subTypes.
     * </p>
     * 
     * @param subTypes
     *            the subTypes to set
     */
    public void setSubTypes(List<DocumentType> subTypes)
    {
        this.subTypes = subTypes;
    }

    /**
     * <p>
     * Getter for query.
     * </p>
     * 
     * @return the query
     */
    public String getQuery()
    {
        return query;
    }

    /**
     * <p>
     * Get the Document Type for the display name.
     * </p>
     * 
     * @param id
     *            id of the type
     * @return {@link DocumentType}
     * 
     */
    public static DocumentType getType(String id)
    {
        if (IWMIUtils.isNullOrEmpty(id))
        {
            return docTypeMap.get(1);
        }
        else
        {
            return docTypeMap.get(Integer.valueOf(id.trim()));
        }
    }

    /**
     * <p>
     * Getter for docTypes.
     * </p>
     * 
     * @return the docTypes
     */
    public static List<DocumentType> getDocTypes()
    {
        return docTypes;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString()
    {
        return this.dispName;
    }

    @Override
    public int compareTo(DocumentType o)
    {
        if (o == null)
            return -1;
        if (this.sortOrder == o.getSortOrder())
        {
            return 0;
        }
        else if (this.sortOrder < o.getSortOrder())
        {
            return -1;
        }
        else if (this.sortOrder > o.getSortOrder())
        {
            return 1;
        }
        return 0;
    }

    public static void main(String[] args)
    {
        DocumentType.loadData();

        for (DocumentType type : DocumentType.getDocTypes())
        {
            System.out.println(":::: -->>" + type.getId() + "::" + type.getDispName());
        }
    }

}
