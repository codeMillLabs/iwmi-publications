package com.hashcode.iwmi.publications.servlets;

import static com.hashcode.iwmi.publications.util.IWMIConst.DOC_TYPE;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.publications.services.AbstractViewHandler;
import com.hashcode.iwmi.publications.services.PublicationListViewHandler;
import com.hashcode.iwmi.publications.util.DocumentType;

/**
 * Servlet implementation class IWMIPublicationsSearchRequestServlet
 */
public class IWMIPublicationsSearchRequestServlet extends HttpServlet
{

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(IWMIPublicationsSearchRequestServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IWMIPublicationsSearchRequestServlet()
    {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
    public void init() throws ServletException
    {
        logger.info("Initializing.......");
        super.init();
        DocumentType.loadData();
        logger.info("Initialized");
    }

    private void doServe(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        try
        {
            logger.info("Request going to process");
            String docTypeReq = request.getParameter(DOC_TYPE.getDesc());
            DocumentType documentType = DocumentType.getType(docTypeReq);
            logger.info("Selected Document Type : {}", documentType);

            AbstractViewHandler viewHandler = new PublicationListViewHandler();
            viewHandler.handleRequest(request, response, documentType);
            logger.info("Request handling done");

            RequestDispatcher dispather = request.getRequestDispatcher("./jsp/publicationsDetailView.jsp");
            dispather.forward(request, response);
        }
        catch (Exception e)
        {
            logger.debug("Error encountered while processing the request,", e);
            logger.error("Error encountered while processing the request, {}", e.getMessage());
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        logger.info("IWMI Publisher, Get Request came");
        doServe(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
        IOException
    {
        logger.info("IWMI Publisher, Post Request came");
        doServe(request, response);
    }

}
