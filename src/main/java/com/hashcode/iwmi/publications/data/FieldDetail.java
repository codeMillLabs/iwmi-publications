/*
 * FILENAME
 *     FieldDetail.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2011 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.publications.data;

import static com.hashcode.iwmi.publications.util.IWMIUtils.getPropertyValue;
import static com.hashcode.iwmi.publications.util.IWMIUtils.isNullOrEmpty;

import java.io.Serializable;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Field Details bean to display in Project details.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class FieldDetail implements Serializable
{

    private static final long serialVersionUID = -3447519695727460477L;

    private String displayName;
    private String fieldName;
    private String fieldValue;
    private boolean captionRequired;

    /**
     * <p>
     * Constructor with all fields.
     * </p>
     * 
     * @param displayName
     *            field display name
     * @param fieldName
     *            solr document field name
     * @param fieldValue
     *            field value
     * @param captionRequired
     *            caption required to show in display
     * 
     */
    public FieldDetail(String fieldName, String displayName, String fieldValue, boolean captionRequired)
    {
        this.fieldName = fieldName;
        this.displayName = displayName;
        this.fieldValue = fieldValue;
        this.captionRequired = captionRequired;
    }

    /**
     * <p>
     * Create a Field Details using metadata.
     * </p>
     * 
     * @param fieldName
     * @param fieldValue
     * @return {@link FieldDetail}
     */
    public static FieldDetail createFieldDetail(String fieldName, String fieldValue)
    {
        String metaData = getPropertyValue("pub.field." + fieldName);
        if (null == metaData)
            return null;
        String[] data = metaData.split(":");
        return new FieldDetail(fieldName, data[1], formatDisplayValues(fieldName, fieldValue), getBoolean(data[2]));
    }

    private static boolean getBoolean(final String bool)
    {
        return ("true".equals(bool)) ? Boolean.TRUE : Boolean.FALSE;
    }

    private static String formatDisplayValues(final String fieldName, final String value)
    {
        if ("filename".equals(fieldName))
        {
            if (!isNullOrEmpty(value.trim()))
            {
                //String pdfDirPath = getPropertyValue("pub.pdf.dir.path");
                String fileNameWithoutExtention = "";
                if (value.lastIndexOf(".") > 0)
                {
                    fileNameWithoutExtention = value.substring(0, value.lastIndexOf("."));
                }
                return fileNameWithoutExtention;
            }
        }

        if ("preview_url".equals(fieldName))
        {
            if (!isNullOrEmpty(value.trim()))
            {
                String imageDirPath = getPropertyValue("pub.images.dir.path");
                String imageName = "";
                if (value.lastIndexOf("/") > 0)
                {
                    imageName = value.substring(value.lastIndexOf("/") + 1, value.length());
                }
                return imageDirPath + imageName.trim();
            }
        }
        
        return value;
    }

    /**
     * <p>
     * Getter for displayName.
     * </p>
     * 
     * @return the displayName
     */
    public String getDisplayName()
    {
        return displayName;
    }

    /**
     * <p>
     * Setting value for displayName.
     * </p>
     * 
     * @param displayName
     *            the displayName to set
     */
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    /**
     * <p>
     * Getter for fieldName.
     * </p>
     * 
     * @return the fieldName
     */
    public String getFieldName()
    {
        return fieldName;
    }

    /**
     * <p>
     * Setting value for fieldName.
     * </p>
     * 
     * @param fieldName
     *            the fieldName to set
     */
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    /**
     * <p>
     * Getter for fieldValue.
     * </p>
     * 
     * @return the fieldValue
     */
    public String getFieldValue()
    {
        return fieldValue;
    }

    /**
     * <p>
     * Setting value for fieldValue.
     * </p>
     * 
     * @param fieldValue
     *            the fieldValue to set
     */
    public void setFieldValue(String fieldValue)
    {
        this.fieldValue = fieldValue;
    }

    /**
     * <p>
     * Getter for captionRequired.
     * </p>
     * 
     * @return the captionRequired
     */
    public boolean isCaptionRequired()
    {
        return captionRequired;
    }

    /**
     * <p>
     * Setting value for captionRequired.
     * </p>
     * 
     * @param captionRequired
     *            the captionRequired to set
     */
    public void setCaptionRequired(boolean captionRequired)
    {
        this.captionRequired = captionRequired;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("FieldDetail [displayName=");
        builder.append(displayName);
        builder.append(", fieldName=");
        builder.append(fieldName);
        builder.append(", fieldValue=");
        builder.append(fieldValue);
        builder.append(", captionRequired=");
        builder.append(captionRequired);
        builder.append("]");
        return builder.toString();
    }

    public static void main(String[] a)
    {
        //        FieldDetail fd = FieldDetail.createFieldDetail("preview_url", "http://publications.iwmi.org/TN/H045831.jpg");
        FieldDetail fd = FieldDetail.createFieldDetail("filename", "H045831.jpg");
        System.out.println(":::::::::  FD :" + fd.displayName);
        System.out.println(":::::::::  FD :" + fd.fieldValue);
    }
}
