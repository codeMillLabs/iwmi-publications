/**
 * 
 */
package com.hashcode.iwmi.publications.data;

/**
 * @author Amila
 * 
 */
public enum SortType {

	ASC("asc"), DESC("desc");

	private String desc;

	private SortType(String d) {
		this.desc = d;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.getDesc();
	}

}
