<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @email amilasilva@hashcodesys.com
 * @version 1.0
 
 --%>


<div id="footer">
	<div id="author">
		Powered by <a target="_blank" href="http://www.hashcodesys.com">hashCode</a>
	</div>
	<div id="copy-right">
		&copy; Copyright 2012 <a target="_blank"
			href="http://www.iwmi.cgiar.org/">IWMI</a>
	</div>
</div>