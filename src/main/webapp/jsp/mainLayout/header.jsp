<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva@hashcodesys.com
 * @version 1.0
  --%>
<%@page import="com.hashcode.iwmi.publications.util.DocumentType"%>
<%@page import="java.util.List"%>

<div id="search-bar">
	<div id="search-buttons">
		<form id="searchForm" action="pubSearchReq" method="post">
			<div class="header-items">
				<div style="float: left">
					<label class="label-style">Search : </label><input type="text"
						name="st"
						<c:choose> 
						<c:when test="${requestScope.st == '' || requestScope.st == null } "> value="author, title, keyword or abstract" </c:when>
						<c:otherwise>value="${requestScope.st}"</c:otherwise></c:choose>
						id="search-text" onclick="removeText(this)" />
				</div>
			</div>
			<div class="clear"></div>
			<div class="header-items">
				<label class="label-style">Document :</label> <select
					id="document-type" name="dt">
					<%
                   DocumentType.loadData();
                   List<DocumentType> docTypeList = DocumentType.getDocTypes();
                
                   for(DocumentType docType : docTypeList) {
                	   int docId = docType.getId();
                	   request.setAttribute("dn1", "" + docId);
                	   if(docType.getSubTypes().isEmpty()) {
                		  
                		  %>
					<option value="<%=docId %>"
						<c:if test="${requestScope.dt ne null && requestScope.dt eq requestScope.dn1}">
                			   selected="selected"
                		    </c:if>><%=docType.getDispName() %></option>
					<%
                	   } else {
                		   %>
					<option disabled="disabled" ><%=docType.getDispName() %></option>
						<%
                		      for(DocumentType subType : docType.getSubTypes()) {
                		    	  int subDocId = subType.getId();
                           	      request.setAttribute("dn2", "" + subDocId);

                		   %>
						<option value="<%=subDocId %>" style="padding-left: 10px"
							<c:if test="${requestScope.dt ne null && requestScope.dt eq requestScope.dn2}">
                		   		selected="selected"
                		  	  </c:if>><%=subType.getDispName() %></option>
						<%
                		      }
                		   %>
					<%
                	   }
                   }
                %>
				</select> <input type="button" name="Search" id="search-btn" value="Search"
					onclick="searchCall()" style="cursor: pointer;" /> <input
					type="reset" name="Reset" id="reset-btn" onclick="resetForm()"
					style="cursor: pointer;" />
					<c:if test="${applicationScope.properties.download eq 'true'}">
					<div id="update-btn">
						<a href="${applicationScope.properties.syncclient}" class='iframe'><img src="images/download.png"></a>
						</div>
					</c:if>
				
			</div>
		</form>
		
	</div>
	<div class="clear"></div>
</div>
<!-- overlayed element -->
<div class="apple_overlay" id="overlay">
	<!-- the external content is loaded inside this tag -->
	<div class="contentWrap"></div>
</div>
<script type="text/javascript">
	function searchCall() {
		var searchFld = document.getElementById("search-text");
		if (searchFld.value == "author, title, keyword or abstract")
			searchFld.value = "";

		var form = document.getElementById("searchForm");
		form.submit();
	}

	function removeText(field) {
		if (field.value == "author, title, keyword or abstract")
			field.value = "";
	}
	
	function resetForm()
	{
		document.getElementById("search-text").value = "author, title, keyword or abstract";
		document.getElementById("document-type").value = "1";
	}
</script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script>
		$(document).ready(function(){
			//Examples of how to assign the ColorBox event to elements
			$(".ajax").colorbox();
			$(".iframe").colorbox({iframe:true, width:"45%", height:"20%"});
		});
	</script>
