<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @email amilasilva@hashcodesys.com
 * @version 1.0
 
 --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@page import="java.io.PrintStream"%>
<%@page import="org.apache.solr.common.SolrDocument"%>
<%@page import="org.apache.solr.common.SolrDocumentList"%>
<%@page import="com.hashcode.iwmi.publications.util.IWMIUtils"%>
<%@page import="com.hashcode.iwmi.publications.util.IWMIConst"%>
<%@page import="com.hashcode.iwmi.publications.data.FieldDetail"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<c:choose>
	<c:when test="${requestScope.no_data == null}">
	<%
				long startPos = (Long) request
						.getAttribute(IWMIConst.START_POINT.getDesc());
				long totalRecs = (Long) request
						.getAttribute(IWMIConst.TOTAL_NO_REC.getDesc());

				List<Map<String, FieldDetail>> displayDataSet = (List<Map<String, FieldDetail>>) request
						.getAttribute(IWMIConst.DATA_SET.getDesc());
				List<String> fieldOrder = (List<String>) request
						.getAttribute(IWMIConst.FIELD_ORDER.getDesc());
	%>
	    <div id="records-summary">Total no of records : <%=totalRecs %></div>
		<div id="details-view">
			<div align="top">
				<div style="min-height: 400px">
					<table class="list-links">
						<tbody>
							<!-- view content goes here -->
							<%
								int c = 0;
										for (Map<String, FieldDetail> item : displayDataSet) {
											c++;
											String className = "";
											if (c % 2 == 1) {
												className = "listTableOdd";
											} else {
												className = "listTableEven";
											}
							%>
							<tr class="<%=className%>">
								<td>
									<table>
										<tr class="<%=className%>">
											<td width="80px">
											<div>
											<%  FieldDetail fileNameField = item.get("filename"); 
											    String imagePath = "";
											    String pdfPath = "";
											    if (fileNameField != null) {
											    	imagePath = IWMIUtils.getPropertyValue("pub.images.dir.path") + fileNameField.getFieldValue() + ".jpg";
											    	
											    	boolean viewOnline = Boolean.valueOf(IWMIUtils.getPropertyValue("preview.pdf.online.enable"));
											    	if(viewOnline) {
											    		FieldDetail urlField = item.get("url"); 
											    		pdfPath = urlField.getFieldValue();
											    	} else {
											    		pdfPath = IWMIUtils.getPropertyValue("pub.pdf.dir.path") + fileNameField.getFieldValue() + ".pdf";
											    	}
											%>
											   <!-- Preview Image --> 
											    <img 
													  src="<%=imagePath %>" alt="" onerror="this.onerror=null;this.src='images/default-image.jpg'" width="75" height="113"/></div>
												<br/>
												<!-- PDF View Link --> 
												 <a
												href="<%=pdfPath %>" target="_blank">View PDF</a> <%} else { %> <img 
													  src="images/default-image.jpg" width="75" height="113"/> <% } %>
											</td>
											<td>
												<!-- publications details -->
												<table>
													<%
                                                       for(String fieldName : fieldOrder) {
                                            	          FieldDetail fieldDetail = item.get(fieldName); 
                                            	          if(fieldDetail == null){
                                            	        	  continue;
                                            	          }
                                          				 %>
													<tr class="<%=className%>">
														<td>
															<% if(fieldName.equals("title")) { %> <b><%=fieldDetail.getFieldValue()%></b>
															<%} else { %> <% if(fieldDetail.isCaptionRequired()) { %> <b><%=fieldDetail.getDisplayName()%>:
														</b> <%} %> <%=fieldDetail.getFieldValue() %> <%} %>
														</td>
													</tr>
													<% } %>
												</table>
											</td>
											<hr/>
										</tr>
									</table>
								</td>
							<tr>
								<% }%>
							</tr>
						</tbody>
					</table>
					<hr/>
				</div>
			</div>
		</div>
		<table
			style="font-size: 12px; font-weight: bold; margin: 0 auto; width: 960px;">
			<tbody>
				<tr>
					<!-- Pagination row -->
					<td colspan="4" align="right"><c:if
							test="${requestScope.hasPrevPg == true}">
							<c:url value="/pubSearchReq" var="prevPgUrl">
								<c:param name="st" value="${requestScope.st}" />
								<c:param name="sp" value="${requestScope.prevPg}" />
								<c:param name="dt" value="${requestScope.dt}" />
							</c:url>
							<a class="pagination-buttons"
								href='<c:out value="${prevPgUrl}"/>'>&lt;&lt;Prev</a>
						</c:if> &nbsp;Page&nbsp;<c:out value="${requestScope.currentPg}" />&nbsp;of&nbsp;
						<c:out value="${requestScope.totalPg}" /> &nbsp; <c:if
							test="${requestScope.hasNextPg == true}">
							<c:url value="/pubSearchReq" var="nextPgUrl">
								<c:param name="st" value="${requestScope.st}" />
								<c:param name="sp" value="${requestScope.nextPg}" />
								<c:param name="dt" value="${requestScope.dt}" />
							</c:url>
							<a class="pagination-buttons"
								href='<c:out value="${nextPgUrl}"/>'>Next&gt;&gt;</a>
						</c:if></td>
					<td><div id="backHomeStyle">
							<div class="backButton">
								<a href="index.jsp">Home</a>
							</div>
						</div></td>
				</tr>
			</tbody>
		</table>

	</c:when>
	<c:otherwise>
		<div class="noRecords">No Records Found</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div id="backHomeStyle">
			<div class="backButton">
				<a href="JavaScript:void(o);" onclick="history.go(-1)">Back</a>
			</div>
		</div>
	</c:otherwise>

</c:choose>