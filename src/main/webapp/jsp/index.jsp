<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva@gmail.com
 * @version 1.0
 
 -->
<html>
<head>
<link rel="icon" type="image/png" href="./images/favicon.png" />
<title>IWMI Resource Viewer</title>
<link rel="stylesheet" href="style.css" />
<script type="text/javascript">
function newPopup(url) {
    var left = (screen.width/2)-(960/2);
    var top = (screen.height/2)-(600/2);

	popupWindow = window.open(
		url,'popUpWindow','height=600,width=960,left='+left+',top='+top+',resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no')

}
</script>

</head>
<body>
	<div id="wrapper">    	
		<%@include file='/jsp/mainLayout/header.jsp'%>
		<div id="search-seperator"></div>
		<div id="content">
			<div class="main-image-container">
				<img src="images/main-image.jpg" width="528px" height="360px" />
                <div align="center">
                	<a href="jsp/why-water.html" class='ajax'><img src="./images/why-iwmi.png"/></a>
                	<a href="jsp/about-iwmi.html" class='ajax'><img style="margin-left:30px;" src="./images/about-iwmi.png"/></a>
                	<a href="http://www.iwmi.cgiar.org" target="_blank"><img style="margin-left:30px;" src="./images/cgiar-web.png"/></a>
                </div>
				<div id="footer-logo">
					<img src="images/footer_logo.jpg" /><img src="images/cgiar_logo.jpg" />
				</div>                
			</div>
			<div class="main-vertical-seperator">&nbsp;</div>
			<div class="main-data-container">
				<div id="main-title-usb">On this USB</div>
				<p style="margin-bottom:5px; font-weight:bold;"> Click here for :</p>
				<ol style="margin:0px 0px 0px 0px;">
                	<li><a href="JavaScript:newPopup('jsp/execute.jsp');">Publications</a></li>
                    <li><a href="JavaScript:newPopup('http://google.com');">Topics</a></li>
                </ol>
                <p style="margin: 7px 0px 5px 0px; font-weight:bold">  Search above for :</p>
                <ol style="margin-top:0px;" start="3">   
                    <li>Water Data</li>                   
                    <li>Research Projects</li>
                    <li>Communications Resources
                    	<ol type="a">
                        	<li>Photos</li>
                            <li>Videos</li>
                            <li>Audios</li>
                            <li>Powerpoints</li>
                            <li>Maps</li>
                            <li>Posters</li>
                            <li>Success Stories</li>
                            <li>Media</li>
                        </ol>
                    </li>
                </ol>
				<div id="update-wrapper">
					<div>
						Click here to download latest<br /> resources to USB
					</div>
					<div id="update-btn">
						<a href="http://localhost:8089/syncclient" class='iframe'><img src="images/download.png"></a>
					</div>
				</div>
				<div style="clear: both;"></div>
				<div id="slogan-wrapper">                 	
					<div id="slogan">Water for a food-secure world</div>                   
					<div id="dot-image">
						<img src="images/dots.jpg" />
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>